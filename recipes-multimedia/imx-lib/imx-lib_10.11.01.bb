include imx-lib.inc

PR = "${INC_PR}"

SRC_URI = "${TTC_MIRROR}/imx-lib-${PV}.tar.gz \
           file://autotools.patch \
          "

SRC_URI[md5sum] = "bae585c8dbebd2610e640ccbe96adf6f"
SRC_URI[sha256sum] = "d8fd1716f7afcb0d40fa0ed19e1fe39810a7e16cc661e55a2e199cc8eae17996"

LIC_FILES_CHKSUM = "file://ipu/mxc_ipu_hl_lib.h;endline=13;md5=a3200cc7a8e0f3c35eda0a72dc043d72"

PACKAGE_ARCH = "${MACHINE_ARCH}"
COMPATIBLE_MACHINE = "(mx5)"
