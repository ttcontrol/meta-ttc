# Copyright (C) 2012 Freescale Semiconductor

DESCRIPTION = "Platform specific libraries for imx platform"
LICENSE = "LGPLv2.1"
SECTION = "multimedia"
DEPENDS = "virtual/kernel"

INC_PR = "r6"

LIC_FILES_CHKSUM = "file://ipu/mxc_ipu_hl_lib.h;endline=13;md5=6c7486b21a8524b1879fa159578da31e"

inherit fsl-eula-unpack autotools

PLATFORM_mx6 = "IMX6Q"
PLATFORM_mx5 = "IMX51"

PARALLEL_MAKE="-j 1"

CFLAGS += "-I${STAGING_INCDIR} -I${STAGING_KERNEL_DIR}/include -D${PLATFORM}"
