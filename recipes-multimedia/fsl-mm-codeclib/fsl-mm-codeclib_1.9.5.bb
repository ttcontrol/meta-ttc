SUMMARY = "Freescale Audio/Video codecs for i.MX51 SoC"
AUTHOR = "Oliver Dillinger <oliver.dillinger@ttcontrol.com>"
HOMEPAGE = "http://www.freescale.com"
PRIORITY = "optional"
LICENSE = "EULA-FSL"
DEPENDS = "virtual/kernel imx-lib"
PROVIDES += "libfslcodec"
PR = "r1"

SRC_URI = "${TTC_MIRROR}/${PN}_${PV}.tar.gz"

SRC_URI[md5sum] = "38a0c639b9203e9cc6868e9111fd81da"
SRC_URI[sha256sum] = "e7b5a5469ec9427d8abfc861c64d5b507c23e9b6bf46e912192cb327add0b0e2"

DEFAULT_PREFERENCE = "1"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
INHIBIT_PACKAGE_STRIP = "1"

LIC_FILES_CHKSUM = "file://docs/EULA.txt;md5=ab04419c277bdd149cb4702b89ee31ff"

do_install() {
        install -m 0755 -d ${D}${includedir} ${D}${libdir} ${D}${includedir}/mm_ghdr

        cd ${S}/ghdr
        install -m 0644 *.h ${D}${includedir}/mm_ghdr
        for dir in `find * -type d`
        do
                install -m 0755 -d ${D}${includedir}/mm_ghdr/$dir
                install -m 0644 `find $dir -iname "*.h" -type f -maxdepth 1` ${D}${includedir}/mm_ghdr/$dir
        done

        install -m 0644 ${S}/release/lib/*.so ${D}${libdir}

	install -m 0755 -d ${D}${libdir}/pkgconfig
	install -m 0644 -t ${D}${libdir}/pkgconfig ${S}/pkgconfig/*.pc 
}

ASSUME_SHLIBS += "lib_aac_dec_arm11_elinux.so:fsl-mm-codeclib lib_aac_dec_arm12_elinux.so:fsl-mm-codeclib \
                  lib_aac_dec_arm9_elinux.so:fsl-mm-codeclib lib_aacplus_dec_arm11_elinux.so:fsl-mm-codeclib \
                  lib_aacplus_dec_arm9_elinux.so:fsl-mm-codeclib lib_asf_parser_arm11_elinux.so:fsl-mm-codeclib \
                  lib_asf_parser_arm9_elinux.so:fsl-mm-codeclib lib_avi_parser_arm11_elinux.so:fsl-mm-codeclib \
                  lib_avi_parser_arm9_elinux.so:fsl-mm-codeclib lib_bmp_dec_arm11_elinux.so:fsl-mm-codeclib \
                  lib_bsac_dec_arm11_elinux.so:fsl-mm-codeclib lib_deinterlace_arm11_elinux.so:fsl-mm-codeclib \
                  lib_downmix_arm11_elinux.so:fsl-mm-codeclib lib_flac_dec_arm11_elinux.so:fsl-mm-codeclib \
                  lib_g.711_dec_arm11_elinux.so:fsl-mm-codeclib lib_g.711_dec_arm9_elinux.so:fsl-mm-codeclib \
                  lib_g.711_enc_arm11_elinux.so:fsl-mm-codeclib lib_g.711_enc_arm9_elinux.so:fsl-mm-codeclib \
                  lib_g.723.1_dec_arm11_elinux.so:fsl-mm-codeclib lib_g.723.1_dec_arm9_elinux.so:fsl-mm-codeclib \
                  lib_g.723.1_enc_arm11_elinux.so:fsl-mm-codeclib lib_g.723.1_enc_arm9_elinux.so:fsl-mm-codeclib \
                  lib_g.726_dec_arm11_elinux.so:fsl-mm-codeclib lib_g.726_dec_arm9_elinux.so:fsl-mm-codeclib \
                  lib_g.726_enc_arm11_elinux.so:fsl-mm-codeclib lib_g.726_enc_arm9_elinux.so:fsl-mm-codeclib \
                  lib_g.729ab_dec_arm9_elinux.so:fsl-mm-codeclib lib_g.729ab_enc_arm9_elinux.so:fsl-mm-codeclib \
                  lib_gif_dec_arm11_elinux.so:fsl-mm-codeclib lib_gif_dec_arm9_elinux.so:fsl-mm-codeclib \
                  lib_H264_dec_arm11_elinux.so:fsl-mm-codeclib lib_H264_dec_arm9_elinux.so:fsl-mm-codeclib \
                  lib_jpeg_dec_arm11_elinux.so:fsl-mm-codeclib lib_jpeg_dec_arm9_elinux.so:fsl-mm-codeclib \
                  lib_jpeg_enc_arm11_elinux.so:fsl-mm-codeclib lib_jpeg_enc_arm9_elinux.so:fsl-mm-codeclib \
                  lib_mp3_dec_arm11_elinux.so:fsl-mm-codeclib lib_mp3_dec_arm12_elinux.so:fsl-mm-codeclib \
                  lib_mp3_dec_arm9_elinux.so:fsl-mm-codeclib lib_mp3_enc_arm11_elinux.so:fsl-mm-codeclib \
                  lib_mp3_enc_arm12_elinux.so:fsl-mm-codeclib lib_mp3_enc_arm9_elinux.so:fsl-mm-codeclib \
                  lib_mp3_parser_arm11_elinux.so:fsl-mm-codeclib lib_mp3_parser_arm9_elinux.so:fsl-mm-codeclib \
                  lib_mp4_parser_arm11_elinux.so:fsl-mm-codeclib lib_mp4_parser_arm9_elinux.so:fsl-mm-codeclib \
                  lib_mpeg2_dec_arm11_elinux.so:fsl-mm-codeclib lib_MPEG4ASP_dec_arm11_elinux.so:fsl-mm-codeclib \
                  lib_MPEG4ASP_dec_arm9_elinux.so:fsl-mm-codeclib libmpeg4_encoder_arm11_ELINUX.so:fsl-mm-codeclib \
                  lib_mpg2_demuxer_arm11_elinux.so:fsl-mm-codeclib lib_nb_amr_dec_arm11_elinux.so:fsl-mm-codeclib \
                  lib_nb_amr_dec_arm9_elinux.so:fsl-mm-codeclib lib_nb_amr_enc_arm11_elinux.so:fsl-mm-codeclib \
                  lib_nb_amr_enc_arm9_elinux.so:fsl-mm-codeclib lib_oggvorbis_dec_arm11_elinux.so:fsl-mm-codeclib \
                  lib_peq_arm11_elinux.so:fsl-mm-codeclib lib_png_dec_arm11_elinux.so:fsl-mm-codeclib \
                  lib_sbc_enc_arm11_elinux.so:fsl-mm-codeclib lib_sbc_enc_arm9_elinux.so:fsl-mm-codeclib \
                  lib_src_ppp_arm11_elinux.so:fsl-mm-codeclib lib_wb_amr_dec_arm11_elinux.so:fsl-mm-codeclib \
                  lib_wb_amr_dec_arm12_elinux.so:fsl-mm-codeclib lib_wb_amr_dec_arm9_elinux.so:fsl-mm-codeclib \
                  lib_wb_amr_enc_arm11_elinux.so:fsl-mm-codeclib lib_wb_amr_enc_arm12_elinux.so:fsl-mm-codeclib \
                  lib_wb_amr_enc_arm9_elinux.so:fsl-mm-codeclib lib_wma10_dec_arm11_elinux.so:fsl-mm-codeclib \
                  lib_wma10_dec_arm12_elinux.so:fsl-mm-codeclib lib_wma10_dec_arm9_elinux.so:fsl-mm-codeclib \
                  lib_wma8_enc_arm11_elinux.so:fsl-mm-codeclib lib_wma_muxer_arm11_ELINUX.so:fsl-mm-codeclib \
                  lib_WMV789_dec_arm11_elinux.so:fsl-mm-codeclib lib_WMV9MP_dec_MP_arm11_elinux.so:fsl-mm-codeclib"

DEFAULT_PREFERENCE = "1"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
INHIBIT_PACKAGE_STRIP = "1"
COMPATIBLE_MACHINE = "(mx5)"

PACKAGE_ARCH = "${MACHINE_ARCH}"

FILES_${PN} += "${libdir}/*${SOLIBS}"
#FILES_${PN}-dbg += "${libdir}/.debug"
FILES_${PN}-dev += "${libdir}/*${SOLIBSDEV} \
                    ${includedir}/*"

