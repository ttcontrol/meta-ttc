FILESEXTRAPATHS := "${THISDIR}/${PN}-${PV}"
SRC_URI_append_mx5 = "\
                      file://gstreamer0.10.36_gstmfwbuffer_v2.patch \
                      file://gstreamer0.10.36_gstmfwbuffer_subbuffer.patch"
PRINC := "${@int(PRINC) + 1}"
