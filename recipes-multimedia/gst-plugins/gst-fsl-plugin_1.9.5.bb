# Copyright (C) 2012-2013 Freescale Semicondutor
# Released under the MIT license (see COPYING.MIT for the terms)

require gst-fsl-plugin.inc

DEPENDS = "gstreamer gst-plugins-base libfslcodec virtual/kernel"
PR = "r1"

SRC_URI = "${TTC_MIRROR}/gst-fsl-plugin_${PV}.tar.gz \
           file://gst-fsl-plugin_1.9.5.diff \
           file://Fix-unknown-type-name-uint.patch \
           file://vsslib-Add-librt-dependency.patch \
           file://fix-nullptr-flow-error.patch \
           file://yuv420.patch \
           file://uyvy.patch \
           file://s_crop.patch \
           file://fix-log-message-v4lsrc.patch \
           file://add-device-dbgmsg-v4lsrc.patch \
           file://vssconfig-vision2.patch \
           file://channels.patch \
           file://fix-isink.patch \
           file://mfw-v4lsrc_fix-memory-leak.patch \
          "

SRC_URI[md5sum] = "d21e3e5e8be93201c4bde02d7851c3ca"
SRC_URI[sha256sum] = "fa8cd981df17d92aa999cd9203af23a9a75adefae1ec5a14ec44a56466319909"

S = "${WORKDIR}/${PN}-${PV}"

COMPATIBLE_MACHINE = "(mx28|mx5)"
