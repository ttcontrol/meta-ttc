# Copyright (C) 2011-2013 Freescale Semiconductor
# Released under the MIT license (see COPYING.MIT for the terms)

PR = "${INC_PR}.24"

include linux-imx.inc

COMPATIBLE_MACHINE = "(mx5)"

SRC_URI = "${TTC_GIT}/linux.git;protocol=${TTC_GIT_PROTO};tag=vision2-1.7.0.0 \
           file://perf-avoid-use-sysroot-headers.patch \
           file://defconfig \
	"

# Revision of imx_2.6.35_maintain branch
SRCREV = "babic"
#LOCALVERSION = "-maintain+yocto"

#SRC_URI += "file://NFS-Fix-nfsroot-support.patch \
#            file://NFS-allow-nfs-root-mount-to-use-alternate-rpc-ports.patch \
#            file://perf-avoid-use-sysroot-headers.patch \
#"
