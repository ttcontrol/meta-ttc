SUMMARY = "fastdroid-vnc Framebuffer VNC Server"
HOMEPAGE = "http://code.google.com/p/fastdroid-vnc"
LICENSE = "GPLv2"

LIC_FILES_CHKSUM = "file://README;md5=65d560bbfaadfe3486b1d9e7e3b13d3a"

SRC_URI = "http://fastdroid-vnc.googlecode.com/files/${PN}-${PV}.tgz \
           file://001-Makefile.patch \
           file://002-Triple-buffer-mmap.patch \
           file://003-Add-keyboard-events.patch \
           file://004-Remove-evdev-search.patch \
           file://005-Disable-touch-support.patch \
           file://006-Change-displayname.patch \
           file://007-Disable-keyboard-support.patch \
          "
SRC_URI[md5sum] = "54f8b873c928aeca6b11f628ff29fe15"
SRC_URI[sha256sum] = "0a955322d2ae35df4364547af96b06ce23c47f720498cda71aac794e07a48a2a"

DEPENDS = "jpeg"

S = "${WORKDIR}/${PN}-${PV}"

do_install () {
	install -d ${D}/usr/bin
	install ${S}/fastdroid-vnc ${D}/usr/bin
}

