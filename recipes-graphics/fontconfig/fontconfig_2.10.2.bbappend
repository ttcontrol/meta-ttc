FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI += "file://local.conf \
           "

do_install_append() {
	install -d ${D}${sysconfdir}/fonts
	install -m 0444 ${WORKDIR}/local.conf ${D}${sysconfdir}/fonts
}

