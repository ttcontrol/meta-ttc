SUMMARY = "The DejaVu fonts - TTF Edition"
DESCRIPTION = "The DejaVu fonts are a font family based on the \
Vera Fonts. Its purpose is to provide a wider range of characters \
while maintaining the original look and feel through the process \
of collaborative development (see authors), under a Free license."
HOMEPAGE = "http://dejavu.sourceforge.net/wiki/"
SECTION = "x11/fonts"
LICENSE = "BitstreamVera"
LIC_FILES_CHKSUM = "file://${WORKDIR}/dejavu-fonts-ttf-${PV}/LICENSE;md5=9f867da7a73fad2715291348e80d0763"

PR = "r7"
RDEPENDS_${PN} = "fontconfig-utils"

inherit fontcache

SRC_URI = "${SOURCEFORGE_MIRROR}/project/dejavu/dejavu/${PV}/dejavu-fonts-ttf-${PV}.tar.bz2 \
	  file://30-dejavu-aliases.conf"

FONT_PACKAGES = "${PN}"

S = "${WORKDIR}/dejavu-fonts-ttf-${PV}/ttf"

do_install () {
	install -d ${D}${prefix}/share/fonts/ttf/
        for i in *.ttf; do
                install -m 644 $i ${D}${prefix}/share/fonts/ttf/${i}
        done

	install -d ${D}${sysconfdir}/fonts/conf.d/
	install -m 0644 ${WORKDIR}/30-dejavu-aliases.conf ${D}${sysconfdir}/fonts/conf.d/
}

FILES_${PN} = "/etc ${datadir}/fonts/ttf ${sysconfdir}/fonts/conf.d/"

SRC_URI[md5sum] = "161462de16e2ca79873bc2b0d2e6c74c"
SRC_URI[sha256sum] = "d0a72c3ae99f5b8d7470daf091ec43f78dc942795aed5c996ab1aa678702eb5d"
