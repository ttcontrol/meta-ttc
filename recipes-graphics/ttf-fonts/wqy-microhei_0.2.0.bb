DESCRIPTION = "WenQuanYi Zen Hei Microi"
AUTHOR = "Qianqian Fang and The WenQuanYi Project Contributors"
HOMEPAGE = "http://wqy.sourceforge.net/en/"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://LICENSE_GPLv3.txt;md5=d32239bcb673463ab874e80d47fae504"
PR = "r0"

RDEPENDS_${PN} = "fontconfig-utils"

inherit fontcache

SRC_URI = "${SOURCEFORGE_MIRROR}/project/wqy/wqy-microhei/${PV}-beta/wqy-microhei-${PV}-beta.tar.gz"

FONT_PACKAGES = "${PN}"

S = "${WORKDIR}/wqy-microhei"

do_install () {
	install -d ${D}${prefix}/share/fonts/ttf/
        for i in *.ttc; do
                install -m 644 $i ${D}${prefix}/share/fonts/ttf/${i}
        done

	install -d ${D}${sysconfdir}/fonts/conf.d/
}

FILES_${PN} = "/etc ${datadir}/fonts/ttf ${sysconfdir}/fonts/conf.d/"

SRC_URI[md5sum] = "a124c5c6606f4f3b733d3477380e9d2f"
SRC_URI[sha256sum] = "2802ac8023aa36a66ea6e7445854e3a078d377ffff42169341bd237871f7213e"
