DESCRIPTION = "Clutter based widget library"
LICENSE = "LGPLv2.1"
PR = "r1"

DEPENDS = "clutter"

inherit autotools

SRC_URI = "${TTC_MIRROR}/${PN}_${PV}.tar.bz2 \
           file://configure.patch"

S = "${WORKDIR}/clutter-ply"

SRC_URI[md5sum] = "66cdbd145c061634bcfdf4c95e62baec"
SRC_URI[sha256sum] = "6a2e09f1893f074de5cbb7e246e3dee84b2bf1c7ce2c0c259641c8d5f042c385"

LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504 \
		    "
