require clutter.inc
require clutter-package.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=7fbc338309ac38fefcd64b04bb903e34"

SRCREV = "43badfc51fb944ca4d9cd922a74b8b02d8e75ff5"
PROVIDES = "clutter"

PV = "1.2.5+git${SRCPV}"
PR = "r3"

DEFAULT_PREFERENCE = "10"

SRC_URI_windows-x86 = "git://git.clutter-project.org/clutter.git;protocol=git;branch=clutter-1.2 \
                       file://v1.2.5_01-red-blue-inversion.patch;patch=1 \
                       file://v1.2.5_02-picking.patch;patch=1 \
                       file://v1.2.5_03-events.patch;patch=1 \
                       file://0004-win32-Use-GCLP_-instead-of-GCL_-when-calling-GetC.patch;patch=1 \
                       file://0061-clutter-event-win32-Directly-enqueue-events-in-mess.patch;patch=1 \
                       file://0062-clutter-event-win32-Emit-multiple-events-for-WM_MOU.patch;patch=1 \
                       file://v1.2.5_07-events-win32.patch;patch=1"

SRC_URI_vision2 =  "git://git.clutter-project.org/clutter.git;protocol=git;branch=clutter-1.2 \
		    file://0001-Fixes-the-red-blue-inverted-color-channels-of-textur.patch;apply=yes \
		    file://0002-Fixes-false-picking-of-stage-only.-Should-be-include.patch;apply=yes \ 
                    file://0003-Fixes-event-processing-touch-screen-in-particular.patch;apply=yes \
                    file://0004-Adjusted-some-flags-in-configure.ac2.patch;apply=yes \
                    file://0005-Map-the-the-up-down-and-enter-keys-of-the-keyboard.patch;apply=yes \
                    file://0008-disable-dithering-like-proposed-by-ATR.patch;apply=yes \
                    file://0009-uniform-key-mapping-for-7-and-10.4-variants.patch;apply=yes \
		    file://0010-improve-key-event-handling.patch;apply=yes \
		    file://0011-Fix-unknown-type-name-G_CONST_RETURN.patch;apply=yes \
                    file://20130517-texture-external-oes-support.patch;apply=yes \
                    file://20130603-gpu-nv21-conversion.patch;apply=yes \
		    file://20140326-choose-rgb565-egl-config.patch;apply=yes \
		    file://20140512-get-ts-coordinates-in-a-loop.patch;apply=yes \
		    file://20140513-fix-ts-release-evt-issue.patch;apply=yes \
                    file://20140728-issue56805-keep-indices_vbo_ref.patch;apply=yes "

S = "${WORKDIR}/git"

BASE_CONF += "--disable-introspection"

#FILESDIR = ${FILE_DIRNAME}/clutter-1.2.5

AUTOTOOLS_AUXDIR = "${S}/build"
