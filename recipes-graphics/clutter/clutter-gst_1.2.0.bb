require recipes-graphics/clutter/clutter-gst.inc

PR = "r1"

LIC_FILES_CHKSUM = "file://COPYING;md5=7fbc338309ac38fefcd64b04bb903e34 \
                    file://clutter-gst/clutter-gst.h;beginline=1;endline=24;md5=95baacba194e814c110ea3bdf25ddbf4"

SRC_URI = "${TTC_MIRROR}/${PN}-${PV}.tar.bz2 \
           file://streaming-texture.patch;apply=yes \
	   file://20130603-dual_extern_oes_video.patch;apply=yes \
           file://20130819_fix_framerate_issue.patch;apply=yes \
           file://20140312-fix-wrong-paint-signal-handlers.patch;apply=yes \
           file://20140321-create-eglimage-at-sink-init.patch \
           file://20140324-add-debug-output-and-improve-cogl-handle-unref.patch \
           file://20140326-fix-dispose-segfault.patch"

DEPENDS += "clutter"

EXTRA_OECONF += "--enable-gtk-doc-html=no"

SRC_URI[md5sum] = "1048815f0c9152edbfdc59a94d0e3b87"
SRC_URI[sha256sum] = "ed3357c10fe82727091616296abd53c752e15c816a4030474f1919c0d347e5d3"

S = "${WORKDIR}/clutter-gst-${PV}"

OE_LT_RPATH_ALLOW = "any"
OE_LT_RPATH_ALLOW[export]="1"
