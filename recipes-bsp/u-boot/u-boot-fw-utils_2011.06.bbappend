SRC_URI =+ "file://fw_env.config \
	    file://20140507-allow-mac-addr-overwrite.patch"
FILESEXTRAPATHS_prepend := "${THISDIR}/${P}:"

do_install_append () {
  install -d ${D}${sysconfdir}
  install -m 755 ${WORKDIR}/fw_env.config ${D}${sysconfdir}/fw_env.config
}
