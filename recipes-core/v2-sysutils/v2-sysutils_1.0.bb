DESCRIPTION = "Some system utilities/scripts for Vision2"
SECTION = "Base"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58"

SRC_URI = "file://cansetup \
           file://caninfo \
	   file://update-bootloader \
           file://update-sysvol \
           file://netset \
           file://netinfo "

FILES_${PN} = "${sbindir}/update-sysvol ${sbindir}/update-bootloader \
               ${bindir}/cansetup \
	       ${bindir}/caninfo \
               ${bindir}/netinfo \
               ${bindir}/netset "

do_install () {
	install -d ${D}/${sbindir}	
	install -m 755 ${WORKDIR}/update-sysvol ${D}/${sbindir}/update-sysvol
	install -m 755 ${WORKDIR}/update-bootloader ${D}/${sbindir}/update-bootloader
	install -d ${D}/${bindir}
	install -m 755 ${WORKDIR}/cansetup ${D}/${bindir}/cansetup
        install -m 755 ${WORKDIR}/caninfo ${D}/${bindir}/caninfo
	install -m 755 ${WORKDIR}/netinfo ${D}/${bindir}/netinfo
	install -m 755 ${WORKDIR}/netset ${D}/${bindir}/netset
}

