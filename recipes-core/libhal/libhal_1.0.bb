DESCRIPTION = "Hardware Abstraction layer for TTControl Vision2"
SECTION = "Base"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58"

SRC_URI = "file://libhal-1.0.tar.bz2"
S = "${WORKDIR}/libhal"

DEPENDS = "glib-2.0 gpsd gstreamer gst-fsl-plugin udev"
RDEPENDS_${PN} = "glib-2.0 gpsd gstreamer gst-fsl-plugin udev"

inherit autotools pkgconfig

FILES_${PN} += "${base_libdir}/udev/*"
