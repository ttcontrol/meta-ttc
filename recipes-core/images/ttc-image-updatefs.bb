DESCRIPTION = "Basic TTControl specific image with clutter support"

include ttc-image-base.bb

USE_DEVFS = "1"

IMAGE_INSTALL += "mx \
clutter-1.2.5 libroxml \
i2c-tools \
gdk-pixbuf-loader-jpeg \
gdk-pixbuf-loader-png \
ttf-dejavu ttf-bitstream-vera \
"
