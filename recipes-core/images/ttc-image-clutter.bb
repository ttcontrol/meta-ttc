DESCRIPTION = "Basic TTControl specific image with clutter support"

include ttc-image-base.bb

IMAGE_INSTALL += "mx \
gstreamer gst-fsl-plugin clutter-gst clutter-ply \
gdk-pixbuf-loader-jpeg \
gdk-pixbuf-loader-tga \
gdk-pixbuf-loader-gif \
gdk-pixbuf-loader-ani \
gdk-pixbuf-loader-ras \
gdk-pixbuf-loader-pnm \
gdk-pixbuf-loader-xpm \
gdk-pixbuf-loader-ico \
gdk-pixbuf-loader-pcx \
gdk-pixbuf-loader-xbm \
gdk-pixbuf-loader-png \
gdk-pixbuf-loader-icns \
gdk-pixbuf-loader-wbmp \
gdk-pixbuf-loader-bmp \
gdk-pixbuf-loader-qtif \
ttf-dejavu wqy-microhei \
ttf-bitstream-vera \
htmldoc \
unzip zip \
"
