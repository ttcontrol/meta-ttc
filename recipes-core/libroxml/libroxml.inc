DESCRIPTION = "Small, fast and powerful xml library"
AUTHOR = "Tristan Lelong <tristan.lelong@libroxml.net>"
HOMEPAGE = "http://www.libroxml.net"
SECTION = "libs"
PRIORITY = "optional"
LICENSE = "LGPLv2.1+"
INC_PR = "r0"

SRC_URI = "http://download.libroxml.net/pool/v2.x/${P}.tar.gz"

inherit autotools pkgconfig
