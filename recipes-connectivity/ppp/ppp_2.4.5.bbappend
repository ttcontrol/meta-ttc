COMPATIBLE_MACHINE = "(vision2)"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}:"

SRC_URI += "file://0001-skip-tty-reconfig.patch \
            file://0002-ppp-2.4.4-mppe-mppc-1.1.patch \
            file://0003-pppd-vision2-config.patch \
            file://0004-ppp-etc.patch"
