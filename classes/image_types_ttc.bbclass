
inherit image_types

IMAGE_TYPES += "v2update"

IMAGE_DEPENDS_v2update = "mtd-utils-native"

IMAGE_CMD_v2update () {
        FSIMAGE_ROOTFS=rootfs.ubifs
        FSIMAGE_APP=application.ubifs
        CFGFILE=ubinize.cfg
        echo "[application-volume]" > $CFGFILE
        echo "mode=ubi" >> $CFGFILE
        echo "vol_id=1" >> $CFGFILE
        echo "vol_type=dynamic" >> $CFGFILE
        echo "vol_name=application" >> $CFGFILE
        echo "vol_size=762MiB" >> $CFGFILE
        echo "image=$FSIMAGE_APP" >> $CFGFILE
        echo "[rootfs-volume]" >> $CFGFILE
        echo "mode=ubi" >> $CFGFILE
        echo "vol_id=2" >> $CFGFILE
        echo "vol_type=dynamic" >> $CFGFILE
        echo "vol_name=rootfs" >> $CFGFILE
        echo "vol_flags=autoresize" >> $CFGFILE
        echo "image=$FSIMAGE_ROOTFS" >> $CFGFILE


        # ==== BIG-MEM ====

        UBI_IMAGE_NAME="${IMAGE_NAME}_0"
	UBI_FILE_NAME="${UBI_IMAGE_NAME}.rootfs.ubi"
        OUTFILE="${DEPLOY_DIR_IMAGE}/${UBI_FILE_NAME}"
        XMLFILE="${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.xml"

        echo "<Config>" > $XMLFILE
	echo "  <Releases>" >> $XMLFILE
        echo "    <Release version=\"${TTC_RELEASE_VERSION}\" path=\"$UBI_FILE_NAME\">" >> $XMLFILE
        echo "      <Compatibility hw-class=\"0x0001,0x0002,0x0004,0x0101,0x0102,0x0201,0x0202\" sw-class=\"${TTC_SW_CLASS}\"/>" >> $XMLFILE
        echo "    </Release>" >> $XMLFILE

        mkfs.ubifs -r ${IMAGE_ROOTFS} -o ${FSIMAGE_ROOTFS}  -m 4096 -e 520192 -c 2004 -v -x none -U && \
        mkfs.ubifs -o ${FSIMAGE_APP}  -m 4096 -e 520192 -c 2004 -v -x none -U && \
        ubinize -o ${OUTFILE} -m 4096 -p 512KiB -s 1024 $CFGFILE

        ln -sf ${OUTFILE} ${DEPLOY_DIR_IMAGE}/${IMAGE_LINK_NAME}_0.ubi
	
        # ==== SMALL-MEM ====

        UBI_IMAGE_NAME="${IMAGE_NAME}_1"
        UBI_FILE_NAME="${UBI_IMAGE_NAME}.rootfs.ubi"
        OUTFILE="${DEPLOY_DIR_IMAGE}/${UBI_FILE_NAME}"

        echo "    <Release version=\"${TTC_RELEASE_VERSION}\" path=\"$UBI_FILE_NAME\">" >> $XMLFILE
        echo "      <Compatibility hw-class=\"0x0003\" sw-class=\"${TTC_SW_CLASS}\"/>" >> $XMLFILE
        echo "    </Release>" >> $XMLFILE
	echo "  </Releases>" >> $XMLFILE
        echo "</Config>" >> $XMLFILE

        mkfs.ubifs -r ${IMAGE_ROOTFS} -o ${FSIMAGE_ROOTFS} -m 2048 -e 129024 -c 3936 -v -x none -U && \
        mkfs.ubifs -o ${FSIMAGE_APP}  -m 2048 -e 129024 -c 3936 -v -x none -U  && \
        ubinize -o ${OUTFILE} -m 2048 -p 128KiB -s 512 $CFGFILE

        ln -sf ${OUTFILE} ${DEPLOY_DIR_IMAGE}/${IMAGE_LINK_NAME}_1.ubi

        ln -sf ${XMLFILE} ${DEPLOY_DIR_IMAGE}/${IMAGE_LINK_NAME}.xml

}
